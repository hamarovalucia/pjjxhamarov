package cz.mendelu.xhamarov.MagoveZHexanu;

public class Skola extends Lokacia {
    private Magia magia;
    public Skola(String nazov, int x, int y, Magia magia) {
        super(nazov, x, y);
        this.magia=magia;
    }

    public void ziskMagie() {
       hrac.pridajMagiu(magia);
    }

    @Override
    void hrajLokaciu() {
       ziskMagie();
    }

    @Override
    int getX() {
        return 0;
    }

    @Override
    int getY() {
        return 0;
    }

    @Override
    String getNazov() {
        return nazov;
    }


}
