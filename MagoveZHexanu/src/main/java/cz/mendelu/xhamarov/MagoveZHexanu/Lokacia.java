package cz.mendelu.xhamarov.MagoveZHexanu;

public abstract class Lokacia {
    protected String nazov;
    protected int x;
    protected int y;
    protected Hrac hrac;

    public Lokacia (String nazov,int x, int y) {
        this.nazov = nazov;
        this.x = x;
        this.y = y;
    }
    abstract void hrajLokaciu();
    abstract int getX();
    abstract int getY();
    //abstract void setPozicia(int noveX, int noveY);

    abstract String getNazov();

    public Hrac getHrac() {
        return hrac;
    }


}
