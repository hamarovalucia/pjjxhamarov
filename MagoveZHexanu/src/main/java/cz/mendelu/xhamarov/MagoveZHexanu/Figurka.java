package cz.mendelu.xhamarov.MagoveZHexanu;

public class Figurka {
    private int x;
    private int y;
    private String farba;

    public Figurka(int x, int y, String farba) {
        this.x = x;
        this.y = y;
        this.farba = farba;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setPozicia(int noveX, int noveY) {
        x=noveX;
        y=noveY;
    }
}
