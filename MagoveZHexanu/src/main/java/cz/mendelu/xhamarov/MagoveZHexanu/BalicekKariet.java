package cz.mendelu.xhamarov.MagoveZHexanu;
import java.util.ArrayList;
import java.util.Random;

public class BalicekKariet {
    ArrayList<Karta> poleKariet = new ArrayList<Karta>();

    public BalicekKariet(Karta karta0,Karta karta1,Karta karta2,Karta karta3,Karta karta4,Karta karta5,Karta karta6,Karta karta7,Karta karta8,Karta karta9,Karta karta10) {
        poleKariet.add(karta0);
        poleKariet.add(karta1);
        poleKariet.add(karta2);
        poleKariet.add(karta3);
        poleKariet.add(karta4);
        poleKariet.add(karta5);
        poleKariet.add(karta6);
        poleKariet.add(karta7);
        poleKariet.add(karta8);
        poleKariet.add(karta9);
        poleKariet.add(karta10);
    }

    public Karta vygenerujKartu() {
        double vstup = (int) (Math.random() * ((10 - 0) + 1)) + 0;
        return poleKariet.get((int) vstup);
    }
}
