package cz.mendelu.xhamarov.MagoveZHexanu;

public class MagickyPortal extends Lokacia {
    int cislo1;
    int cislo2;
    public MagickyPortal(String nazov, int x, int y, int cislo1, int cislo2) {
        super(nazov, x, y);
        this.cislo1=cislo1;
        this.cislo2=cislo2;
    }

    @Override
    void hrajLokaciu() {
        if (hrac.hodKockou() == cislo1 | hrac.hodKockou() == cislo2) {
            hrac.ukonciTah();
        }
    }

    @Override
    int getX() {
        return x;
    }

    @Override
    int getY() {
        return y;
    }

    @Override
    String getNazov() {
        return nazov;
    }


}
