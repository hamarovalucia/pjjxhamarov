package cz.mendelu.xhamarov.MagoveZHexanu;

public class Mociar extends Lokacia {
    private Hra hra;
    public Mociar(String nazov, int x, int y) {
        super(nazov, x, y);
    }

    @Override
    void hrajLokaciu() {
        if (hrac.hodilSestku()==true) {
            //hra.hrajTah();
        }else{
            vyplatenie();
        }
    }

    public void vyplatenie() {
        if (hrac.hodilSestku()==false) {
            //hrac.zahodKartu();
        }else{
            //hrac.ponechajKartu();
            hrac.ukonciTah();
        }
    }

    @Override
    int getX() {
        return 0;
    }

    @Override
    int getY() {
        return 0;
    }

    @Override
    String getNazov() {
        return nazov;
    }


}
