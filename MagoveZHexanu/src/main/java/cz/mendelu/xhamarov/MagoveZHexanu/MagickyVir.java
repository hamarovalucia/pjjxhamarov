package cz.mendelu.xhamarov.MagoveZHexanu;

public class MagickyVir extends Lokacia {
    public MagickyVir(String nazov, int x, int y) {
        super(nazov, x, y);
    }

    @Override
    void hrajLokaciu() {
       hrac.odoberMagiu();
    }

    @Override
    int getX() {
        return x;
    }

    @Override
    int getY() {
        return y;
    }

    @Override
    String getNazov() {
        return nazov;
    }


}
