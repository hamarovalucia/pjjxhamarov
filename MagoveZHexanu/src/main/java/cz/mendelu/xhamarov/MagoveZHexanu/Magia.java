package cz.mendelu.xhamarov.MagoveZHexanu;

public enum Magia {
    MOC,ZEM,KLAM,OBRANA,POHYB,ZIVLY;

    private boolean objavene = false;

    public boolean objaveneHracom() {
        return objavene;
    }

    public void setObjaveneHracom(boolean objavene) {
        this.objavene=objavene;
    }
}
