package cz.mendelu.xhamarov.MagoveZHexanu;

public class MagickaKniha extends Lokacia {
    private BalicekKariet balicekKariet;

    public MagickaKniha(String nazov, int x, int y) {
        super(nazov, x, y);
    }

    public void ziskKarty() {
       hrac.ziskKarty(balicekKariet.vygenerujKartu());
    }

    @Override
    void hrajLokaciu() {
       ziskKarty();
    }

    @Override
    int getX() {
        return x;
    }

    @Override
    int getY() {
        return y;
    }

    @Override
    String getNazov() {
        return nazov;
    }


}
