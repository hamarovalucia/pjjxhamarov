package cz.mendelu.xhamarov.MagoveZHexanu;

import java.util.Arrays;
import java.util.Objects;

public class Hra {
    private BalicekKariet balicekKariet;
    private Lokacia lokacia;
    private static Hrac[] poleHracov = new Hrac[6];
    private int[] polePrekliatychCisel = new int[5];
    private boolean overenieKonca;
    private Object Figurka;
    private cz.mendelu.xhamarov.MagoveZHexanu.Figurka[] poleFiguriek = new Figurka[18];

    public Hra vytvorHru() {
            Figurka zlta1 = new Figurka(0, 0, "zlta");
            Figurka zlta2 = new Figurka(0, 0, "zlta");
            Figurka zlta3 = new Figurka(0, 0, "zlta");
            Figurka modra1 = new Figurka(0, 0, "modra");
            Figurka modra2 = new Figurka(0, 0, "modra");
            Figurka modra3 = new Figurka(0, 0, "modra");
            Figurka cervena1 = new Figurka(0, 0, "cervena");
            Figurka cervena2 = new Figurka(0, 0, "cervena");
            Figurka cervena3 = new Figurka(0, 0, "cervena");
            Figurka zelena1 = new Figurka(0, 0, "zelena");
            Figurka zelena2 = new Figurka(0, 0, "zelena");
            Figurka zelena3 = new Figurka(0, 0, "zelena");
            Figurka ruzova1 = new Figurka(0, 0, "ruzova");
            Figurka ruzova2 = new Figurka(0, 0, "ruzova");
            Figurka ruzova3 = new Figurka(0, 0, "ruzova");
            Figurka biela1 = new Figurka(0, 0, "biela");
            Figurka biela2 = new Figurka(0, 0, "biela");
            Figurka biela3 = new Figurka(0, 0, "biela");

            Hrac hrac1 = new Hrac(Postava.ELANOR, zelena1, zelena2, zelena3);
            Hrac hrac2 = new Hrac(Postava.DARSIN, zlta1, zlta2, zlta3);
            Hrac hrac3 = new Hrac(Postava.ELANOR, modra1, modra2, modra3);
            Hrac hrac4 = new Hrac(Postava.DARSIN, cervena1, cervena2, cervena3);
            Hrac hrac5 = new Hrac(Postava.ELANOR, ruzova1, ruzova2, ruzova3);
            Hrac hrac6 = new Hrac(Postava.DARSIN, biela1, biela2, biela3);
            poleHracov[0] = hrac1;
            poleHracov[1] = hrac2;
            poleHracov[2] = hrac3;
            poleHracov[3] = hrac4;
            poleHracov[4] = hrac5;
            poleHracov[5] = hrac6;

            Lokacia skolaMoci = new Skola("cz.mendelu.xhamarov.MagoveZHexanu.Skola moci", 2, 2, Magia.MOC);
            Lokacia skolaKlamu = new Skola("cz.mendelu.xhamarov.MagoveZHexanu.Skola klamu", -1, 3, Magia.KLAM);
            Lokacia skolaZeme = new Skola("cz.mendelu.xhamarov.MagoveZHexanu.Skola zeme", -3, -3, Magia.ZEM);
            Lokacia skolaZivlov = new Skola("cz.mendelu.xhamarov.MagoveZHexanu.Skola zivlov", 1, -3, Magia.ZIVLY);
            Lokacia skolaObrany = new Skola("cz.mendelu.xhamarov.MagoveZHexanu.Skola obrany", 3, -1, Magia.OBRANA);
            Lokacia skolaPohybu = new Skola("cz.mendelu.xhamarov.MagoveZHexanu.Skola pohybu", -1, -1, Magia.POHYB);

            Lokacia mociar1 = new Mociar("cz.mendelu.xhamarov.MagoveZHexanu.Mociar 1", -1, -3);
            Lokacia mociar2 = new Mociar("cz.mendelu.xhamarov.MagoveZHexanu.Mociar 2", 1, -5);

            Lokacia magickaKniha1 = new MagickaKniha("Magicka kniha 1", 0, -6);
            Lokacia magickaKniha2 = new MagickaKniha("Magicka kniha 2", -3, 3);

            Lokacia magickyVir1 = new MagickyVir("Magicka vir 1", -2, 0);
            Lokacia magickyVir2 = new MagickyVir("Magicka vir 2", 1, 1);
            throw new UnsupportedOperationException("Neni hotova.");
    }

    public void hrajTah(Hrac hrac) {
        throw new UnsupportedOperationException("Neni hotova.");
    }

    public void hrajKolo() {
        for (int i = 0; i < poleHracov.length; i++) {
            if (overenieKonca() == true) {
                koniecHry();
            }else{
                hrajTah(poleHracov[i]);
            }
        }
    }

    public void koniecHry() {

    }

    public boolean overenieKonca() {
        for (int i = 0; i < poleHracov.length; i++) {
            if (poleHracov[i].getPoleMagii().size() == 6) {
                overenieKonca= true;
            } else {
                overenieKonca= false;
            }
        }
        return overenieKonca;
    }

    public void naplneniePolaSPrekliatymiCislami() {
        for (int i = 0; i < polePrekliatychCisel.length; i++) {
            polePrekliatychCisel[i]=poleHracov[i].vyberPrekliateCislo();
        }
    }

    public int[] getPolePrekliatychCisel() {
        return polePrekliatychCisel;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hra hra = (Hra) o;
        return overenieKonca == hra.overenieKonca &&
                Objects.equals(balicekKariet, hra.balicekKariet) &&
                Objects.equals(lokacia, hra.lokacia) &&
                Arrays.equals(polePrekliatychCisel, hra.polePrekliatychCisel) &&
                Objects.equals(Figurka, hra.Figurka) &&
                Arrays.equals(poleFiguriek, hra.poleFiguriek);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(balicekKariet, lokacia, overenieKonca, Figurka);
        result = 31 * result + Arrays.hashCode(polePrekliatychCisel);
        result = 31 * result + Arrays.hashCode(poleFiguriek);
        return result;
    }

    @Override
    public String toString() {
        return "Hra{" +
                "balicekKariet=" + balicekKariet +
                ", lokacia=" + lokacia +
                ", polePrekliatychCisel=" + Arrays.toString(polePrekliatychCisel) +
                ", overenieKonca=" + overenieKonca +
                ", Figurka=" + Figurka +
                ", poleFiguriek=" + Arrays.toString(poleFiguriek) +
                '}';
    }
}
