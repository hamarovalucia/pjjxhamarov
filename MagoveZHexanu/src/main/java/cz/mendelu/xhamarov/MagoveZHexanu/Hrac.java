package cz.mendelu.xhamarov.MagoveZHexanu;
import java.util.*;

public class Hrac {
    private Postava postava;
    private Karta[] poleKariet = new Karta[4];
    private HashSet<Magia> poleMagii = new HashSet<Magia>();
    private Figurka[] poleFiguriek = new Figurka[3];
    private BalicekKariet balicekKariet;

    public Hrac(Postava postava,Figurka figurka0, Figurka figurka1, Figurka figurka2) {
        this.postava=postava;
        poleFiguriek[0]=figurka0;
        poleFiguriek[1]=figurka1;
        poleFiguriek[2]=figurka2;
    }

    public Postava getPostava() {
        return postava;
    }

    public void ziskKarty(Karta akej) {
        for (int i = 0; i < poleKariet.length; i++) {
            if (poleKariet[i] == null) {
                poleKariet[i] = akej;
            } else {
                System.out.println("Nemozno ziskat dalsiu kartu");
            }
        }
    }

    public void pouziKartu(Karta aku, int pozicia) {
        poleKariet[pozicia] = null;
    }

    public void zahodKartu(Karta aku, int pozicia) {
        poleKariet[pozicia] = null;
    }

    public void ponechajKartu(Karta aku) {

    }

    public boolean maMagiu(Magia m) {
        for (Magia vPoli : poleMagii) {
            if (m == vPoli) {
                return true;
            }
        }
        return false;
    }

    public boolean maKartu(Karta k) {
        for (Karta vPoli : poleKariet) {
            if (k == vPoli) {
                return true;
            }
        }
        return false;
    }


    public void pridajMagiu(Magia aku) {
        if (maMagiu(aku) == false) {
                poleMagii.add(aku);
        }else{
            System.out.println("Uz ju ma.");
        }
    }

    public void odoberMagiu() {
        poleMagii.remove(vyberMagiu());
    }

    public void presunFigurku() {
        int x; int y;
        x=vyberFigurku().getX();
        y=vyberFigurku().getY();
        switch (hodKockou()) {
            case 1:
                vyberFigurku().setPozicia(x+1,y+1);
                break;
            case 2:
                vyberFigurku().setPozicia(x+1,y-1);
                break;
            case 3:
                vyberFigurku().setPozicia(x+0,y-2);
                break;
            case 4:
                vyberFigurku().setPozicia(x-1,y-1);
                break;
            case 5:
                vyberFigurku().setPozicia(x-1,y+1);
                break;
            case 6:
                vyberFigurku().setPozicia(x+0,y+2);
                break;
        }

    }

    public Figurka vyberFigurku() {
        return poleFiguriek[0];  //osetrit
    }

    public Magia vyberMagiu() {
        return null; //osetrit
    }

    public int hodKockou() {
        double vstup = (int) (Math.random() * ((6 - 1) + 1)) + 1;
        return (int)vstup;
    }

    public boolean hodilSestku() {
        boolean hodil;
        if (hodKockou()==6) {
            hodil=true;
        }else{
            hodil=false;
        }
        return hodil;
    }

    public void ukonciTah() {

    }

    public int vyberPrekliateCislo() {
        //osetrit aby nemali hraci rovnake
        double vstup = (int) (Math.random() * ((6 - 1) + 1)) + 1;
        return (int)vstup;
    }

    public void pridelFarbu() {

    }

    public HashSet<Magia> getPoleMagii() {
        return poleMagii;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hrac hrac = (Hrac) o;
        return postava == hrac.postava &&
                Arrays.equals(poleKariet, hrac.poleKariet) &&
                Objects.equals(poleMagii, hrac.poleMagii) &&
                Arrays.equals(poleFiguriek, hrac.poleFiguriek) &&
                Objects.equals(balicekKariet, hrac.balicekKariet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(postava, poleMagii, balicekKariet);
        result = 31 * result + Arrays.hashCode(poleKariet);
        result = 31 * result + Arrays.hashCode(poleFiguriek);
        return result;
    }

    @Override
    public String toString() {
        return "Hrac{" +
                "postava=" + postava +
                ", poleKariet=" + Arrays.toString(poleKariet) +
                ", poleMagii=" + poleMagii +
                ", poleFiguriek=" + Arrays.toString(poleFiguriek) +
                ", balicekKariet=" + balicekKariet +
                '}';
    }
}
