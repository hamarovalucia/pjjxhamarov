package cz.mendelu.xhamarov.MagoveZHexanu;

public class Univerzita extends Lokacia {
    private Hra hra;
    public Univerzita(String nazov, int x, int y) {
        super(nazov, x, y);
    }

    @Override
    void hrajLokaciu() {
        hra.naplneniePolaSPrekliatymiCislami();
        if (vydarenieHodu()==true) {
            //hrac.pridajMagiu();  //osetrit podla vyberu
        }else{
            hrac.ukonciTah();
        }
    }

    public boolean vydarenieHodu() {
        boolean vydarenie = false;
        for (int i = 0; i < hra.getPolePrekliatychCisel().length; i++) {
            if (hrac.hodKockou() != hra.getPolePrekliatychCisel()[i]) {
                vydarenie = true;
            } else {
                vydarenie = false;
                break;
            }
        }
        return vydarenie;
    }

    @Override
    int getX() {
        return 0;
    }

    @Override
    int getY() {
        return 0;
    }

    @Override
    String getNazov() {
        return nazov;
    }

}
