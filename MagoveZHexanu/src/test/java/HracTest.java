import cz.mendelu.xhamarov.MagoveZHexanu.Figurka;
import cz.mendelu.xhamarov.MagoveZHexanu.Hrac;
import cz.mendelu.xhamarov.MagoveZHexanu.Magia;
import cz.mendelu.xhamarov.MagoveZHexanu.Postava;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

//pripravna cast- setup
class HracTest {
    Hrac hrac;
    Figurka zelena1;
    Figurka zelena2;
    Figurka zelena3;

    @BeforeEach
    void setup() {
        hrac= new Hrac(Postava.ELANOR,zelena1,zelena2,zelena3);
        zelena1 = new Figurka(0,0,"zelena");
        zelena2 = new Figurka(0,0,"zelena");
        zelena3 = new Figurka(0,0,"zelena");

    }
    /*
    Metoda hodKockou() vygeneruje cislo <1,6>.
    Vyuzije sa pri presune figurky.
    */
    @org.junit.jupiter.api.Test
    void hodKockou() {
        //priprava prostredia- when
        int hod = hrac.hodKockou();
        //asserty- then
        assertTrue(1 <= hod);
        assertTrue(hod <= 6);
    }
    /*
    Metoda maMagiu() sa vyuziva na overenie toho, ci hrac vlastni danu magiu.
    Teda ci sa dana magia nachadza v poliMagii hraca.
    Vyuzije sa ako sucast inej metody, pretoze sa musi zaistit aby hrac nemal v poli tu istu magiu viackrat.
    */
    @Test
    void maMagiu_Klamu_vysledok_true() {
        //setup
        hrac.pridajMagiu(Magia.KLAM);
        //when
        boolean maM = hrac.maMagiu(Magia.KLAM);
        //then
        assertEquals(maM,true);

    }

    @Test
    void maMagiu_Moci_vysledok_false() {
        //setup
        hrac.pridajMagiu(Magia.KLAM);
        //when
        boolean maM = hrac.maMagiu(Magia.MOC);
        //then
        assertEquals(maM,false);

    }
    /*
    Metoda hodilSestku() vrati hodnotu true/false podla toho, ci hrac hodil 6.
    Vyuzije sa v lokacii cz.mendelu.xhamarov.MagoveZHexanu.Mociar, kde hrac musi hodit sestku aby sa odtial dostal.
    */

    @Test
    void hodilSestku() {
        if (hrac.hodKockou() == 6) {
            boolean hodil6 = hrac.hodilSestku();
            assertEquals(hodil6, true);
        }
    }

    @Test
    void hodilSestku_nehodil() {
        if (hrac.hodKockou() == 4) {
            boolean hodil6 = hrac.hodilSestku();
            assertEquals(hodil6, false);
        }
    }
}