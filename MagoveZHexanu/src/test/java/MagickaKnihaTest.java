import cz.mendelu.xhamarov.MagoveZHexanu.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MagickaKnihaTest {

    BalicekKariet balicekKariet;
    Karta karta1,karta2,karta3,karta4,karta5,karta6,karta7,karta8,karta9,karta10,karta11;
    Hrac hrac;
    Figurka zelena1;
    Figurka zelena2;
    Figurka zelena3;

    @BeforeEach
    void setup() {
        karta1= new Magozer();
        karta2= new ImaginarnyMag();
        karta3= new MagickyStit();
        karta4= new NulovaMagia();
        karta5= new SpravnySmer();
        karta6= new Sputanie();
        karta7= new Vymena();
        karta8= new VymenaSkusenosti();
        karta9= new ZdanlivaSila();
        karta10= new ZdanlivaSlabost();
        karta11= new Zemetrasenie();

        balicekKariet = new BalicekKariet(karta1,karta2,karta3,karta4,karta5,karta6,karta7,karta8,karta9,karta10,karta11);

        hrac= new Hrac(Postava.ELANOR,zelena1,zelena2,zelena3);
        zelena1 = new Figurka(0,0,"zelena");
        zelena2 = new Figurka(0,0,"zelena");
        zelena3 = new Figurka(0,0,"zelena");
    }
    /*
    Metoda ziskKarty() prida danu kartu do polaKariet u hraca.
     */
    @Test
    void ziskKarty() {
        //setup
        //when
        hrac.ziskKarty(karta1);   //pridanie karty do pola
        //then
        assertTrue(hrac.maKartu(karta1));  //kontrola ci sa v nom naozaj nachadza
    }

    @Test
    void ziskKarty_nemaKartu() {
        //setup
        //when
        hrac.ziskKarty(karta1);
        //then
        assertFalse(hrac.maKartu(karta2));
    }
}