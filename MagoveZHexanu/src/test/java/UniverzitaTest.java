import cz.mendelu.xhamarov.MagoveZHexanu.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniverzitaTest {
    Hra hra;
    Hrac hrac1;
    Hrac hrac2;
    Figurka zelena1;
    Figurka zelena2;
    Figurka zelena3;
    Figurka zlta1;
    Figurka zlta2;
    Figurka zlta3;
    Univerzita u;

    @BeforeEach
    void setup() {
        hrac1= new Hrac(Postava.ELANOR,zelena1,zelena2,zelena3);
        hrac2= new Hrac(Postava.DARSIN,zlta1,zlta2,zlta3);
        zelena1 = new Figurka(0,0,"zelena");
        zelena2 = new Figurka(0,0,"zelena");
        zelena3 = new Figurka(0,0,"zelena");
        zlta1 = new Figurka(0,0,"zlta");
        zlta2 = new Figurka(0,0,"zlta");
        zlta3 = new Figurka(0,0,"zlta");
        u = new Univerzita("cz.mendelu.xhamarov.MagoveZHexanu.Univerzita",4,2);
    }
    /*
    Metoda vydarenieHodu() sa vyuzije v lokacii Univerzite, kde moze hrac ziskat magiu.
    Ostatni hraci si vyberaju prekliate cislo.
    cz.mendelu.xhamarov.MagoveZHexanu.Hrac ziska magiu len vtedy, ak hodi ine cislo nez zvolili ostatni hraci v hre.
    Zatial som este tuto hru neosetrila tak aby mohli hrat viaceri hraci. Preto to skusam len na jednom hracovi.
     */
    @Test
    void vydarenieHodu() {
        //setup
        int[] polePrekliatychCisel = new int[5];
        polePrekliatychCisel[0]=1;
        polePrekliatychCisel[1]=5;
        polePrekliatychCisel[2]=1;
        polePrekliatychCisel[3]=3;
        polePrekliatychCisel[4]=2;
        boolean vydarenie = false;
        //when
        for (int i = 0; i < polePrekliatychCisel.length; i++) {
            if (4 != polePrekliatychCisel[i]) {
                vydarenie = true;
            } else {
                vydarenie = false;
                break;
            }
        }
        //then
           assertEquals(vydarenie,true);
        }

    @Test
    void vydarenieHodu_False() {
        //setup
        int[] polePrekliatychCisel = new int[5];
        polePrekliatychCisel[0]=1;
        polePrekliatychCisel[1]=5;
        polePrekliatychCisel[2]=1;
        polePrekliatychCisel[3]=3;
        polePrekliatychCisel[4]=2;
        boolean vydarenie = false;
        //when
        for (int i = 0; i < polePrekliatychCisel.length; i++) {
            if (5 != polePrekliatychCisel[i]) {
                vydarenie = true;
            } else {
                vydarenie = false;
                break;
            }
        }
        //then
            assertEquals(vydarenie,false);
    }
}